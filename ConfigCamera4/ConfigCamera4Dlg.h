
// ConfigCamera4Dlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CConfigCamera4Dlg 对话框
class CConfigCamera4Dlg : public CDialogEx
{
// 构造
public:
	CConfigCamera4Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_CONFIGCAMERA4_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edFrontHeight;
	CEdit m_edFrontWidth;
	CEdit m_edFrontHOffset;
	CEdit m_edFrontWOffset;
	CSpinButtonCtrl m_spFrontHeight;
	CSpinButtonCtrl m_spFrontWidth;
	CSpinButtonCtrl m_spFrontHOffset;
	CSpinButtonCtrl m_spFrontWOffset;
	afx_msg void OnDeltaposSpinFrontheight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinFrontwidth(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinFronthoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinFrontwoffset(NMHDR *pNMHDR, LRESULT *pResult);

private:
	double m_fFrontHeight;
	double m_fFrontWidth;
	double m_fFrontHOffset;
	double m_fFrontWOffset;
	
	double m_fBackHeight;
	double m_fBackWidth;
	double m_fBackHOffset;
	double m_fBackWOffset;
	
	double m_fLeftHeight;
	double m_fLeftWidth;
	double m_fLeftHOffset;
	double m_fLeftWOffset;
	
	double m_fRightHeight;
	double m_fRightWidth;
	double m_fRightHOffset;
	double m_fRightWOffset;

	double m_fInnerHeight;
	double m_fInnerWidth;
	double m_fOutterHeight;
	double m_fOutterWidth;

	int sock;
	struct sockaddr_in peerAddr;
	
public:
	afx_msg void OnBnClickedOk();
	CEdit m_edLeftHeight;
	CEdit m_edLeftWidth;
	CEdit m_edLeftHOffset;
	CEdit m_edLeftWOffset;
	CSpinButtonCtrl m_spLeftHeight;
	CSpinButtonCtrl m_spLeftWidth;
	CSpinButtonCtrl m_spLeftHOffset;
	CSpinButtonCtrl m_spLeftWOffset;
	CEdit m_edRightHeight;
	CEdit m_edRightWidth;
	CEdit m_edRightHOffset;
	CEdit m_edRightWOffset;
	CSpinButtonCtrl m_spRightHeight;
	CSpinButtonCtrl m_spRightWidth;
	CSpinButtonCtrl m_spRightHOffset;
	CSpinButtonCtrl m_spRightWOffset;
	CEdit m_edInnerHeight;
	CEdit m_edInnerWidth;
	CEdit m_edOutterHeight;
	CEdit m_edOutterWidth;
	CEdit m_edBackHeight;
	CEdit m_edBackWidth;
	CEdit m_edBackHOffset;
	CEdit m_edBackWOffset;
	CSpinButtonCtrl m_spBackHeight;
	CSpinButtonCtrl m_spBackWidth;
	CSpinButtonCtrl m_spBackHOffset;
	CSpinButtonCtrl m_spBackWOffset;
	afx_msg void OnDeltaposSpinLeftheight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinLeftwidth(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinLefthoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinLeftwoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinBackheight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinBackwidth(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinBackhoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinBackwoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinRightheight(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinRightwidth(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinRighthoffset(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinRightwoffset(NMHDR *pNMHDR, LRESULT *pResult);
	void SendFrontParams(void);
	void SendBackParams(void);
	void SendLeftParams(void);
	void SendRightParams(void);
	afx_msg void OnBnClickedButtonConfrect();
	afx_msg void OnBnClickedButtonReload();
	afx_msg void OnBnClickedButtonSave();
	void Reload(void);
	void SaveConfig(void);
	afx_msg void OnBnClickedButtonFconf();
	afx_msg void OnBnClickedButtonLconf();
	afx_msg void OnBnClickedButtonRconf();
	afx_msg void OnBnClickedButtonBconf();
//	bool m_bFrontComp;
//	bool m_bBackComp;
//	bool m_bLeftComp;
//	bool m_bRightComp;
	afx_msg void OnBnClickedCheckFrontcomp();
	int SendCompConfig(void);
	afx_msg void OnBnClickedCheckBackcomp();
	afx_msg void OnBnClickedCheckLeftcomp();
	afx_msg void OnBnClickedCheckRightcomp();
	BOOL m_bBackComp;
	BOOL m_bFrontComp;
	BOOL m_bLeftComp;
	BOOL m_bRightComp;
};
