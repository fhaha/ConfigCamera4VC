
// ConfigCamera4Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ConfigCamera4.h"
#include "ConfigCamera4Dlg.h"
#include "afxdialogex.h"
#include "json/json.h"
#include "camera4/mavlink.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CConfigCamera4Dlg 对话框
#pragma comment(lib,"json_vc71_libmtd.lib")


std::string
readInputTestFile( const char *path )
{
   FILE *file ;
   errno_t er = fopen_s(&file, path, "rb" );
   if (er ||  !file )
      return std::string("");
   fseek( file, 0, SEEK_END );
   long size = ftell( file );
   fseek( file, 0, SEEK_SET );
   std::string text;
   char *buffer = new char[size+1];
   buffer[size] = 0;
   if ( fread( buffer, 1, size, file ) == (unsigned long)size )
      text = buffer;
   fclose( file );
   delete[] buffer;
   return text;
}

CConfigCamera4Dlg::CConfigCamera4Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CConfigCamera4Dlg::IDD, pParent)
	, m_bBackComp(FALSE)
	, m_bFrontComp(FALSE)
	, m_bLeftComp(FALSE)
	, m_bRightComp(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_fFrontHeight = 0;
	m_fFrontWidth = 0;
	m_fFrontHOffset = 0;
	m_fFrontWOffset = 0;

	m_fBackHeight = 0;
	m_fBackWidth = 0;
	m_fBackHOffset = 0;
	m_fBackWOffset = 0;

	m_fLeftHeight = 0;
	m_fLeftWidth = 0;
	m_fLeftHOffset = 0;
	m_fLeftWOffset = 0;

	m_fRightHeight = 0;
	m_fRightWidth = 0;
	m_fRightHOffset = 0;
	m_fRightWOffset = 0;

	m_fInnerHeight = 0;
	m_fInnerWidth = 0;
	m_fOutterHeight = 0;
	m_fOutterWidth = 0;

	sock = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP);
	
	memset(&peerAddr, 0, sizeof(peerAddr));
	peerAddr.sin_family = AF_INET;
	peerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	peerAddr.sin_port = htons(14551);
}

void CConfigCamera4Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_FRONTHEIGHT, m_edFrontHeight);
	DDX_Control(pDX, IDC_EDIT_FRONTWIDTH, m_edFrontWidth);
	DDX_Control(pDX, IDC_EDIT_FRONTHOFFSET, m_edFrontHOffset);
	DDX_Control(pDX, IDC_EDIT_FRONTWOFFSET, m_edFrontWOffset);
	DDX_Control(pDX, IDC_SPIN_FRONTHEIGHT, m_spFrontHeight);
	DDX_Control(pDX, IDC_SPIN_FRONTWIDTH, m_spFrontWidth);
	DDX_Control(pDX, IDC_SPIN_FRONTHOFFSET, m_spFrontHOffset);
	DDX_Control(pDX, IDC_SPIN_FRONTWOFFSET, m_spFrontWOffset);
	DDX_Control(pDX, IDC_EDIT_LEFTHEIGHT, m_edLeftHeight);
	DDX_Control(pDX, IDC_EDIT_LEFTWIDTH, m_edLeftWidth);
	DDX_Control(pDX, IDC_EDIT_LEFTHOFFSET, m_edLeftHOffset);
	DDX_Control(pDX, IDC_EDIT_LEFTWOFFSET, m_edLeftWOffset);
	DDX_Control(pDX, IDC_SPIN_LEFTHEIGHT, m_spLeftHeight);
	DDX_Control(pDX, IDC_SPIN_LEFTWIDTH, m_spLeftWidth);
	DDX_Control(pDX, IDC_SPIN_LEFTHOFFSET, m_spLeftHOffset);
	DDX_Control(pDX, IDC_SPIN_LEFTWOFFSET, m_spLeftWOffset);
	DDX_Control(pDX, IDC_EDIT_RIGHTHEIGHT, m_edRightHeight);
	DDX_Control(pDX, IDC_EDIT_RIGHTWIDTH, m_edRightWidth);
	DDX_Control(pDX, IDC_EDIT_RIGHTHOFFSET, m_edRightHOffset);
	DDX_Control(pDX, IDC_EDIT_RIGHTWOFFSET, m_edRightWOffset);
	DDX_Control(pDX, IDC_SPIN_RIGHTHEIGHT, m_spRightHeight);
	DDX_Control(pDX, IDC_SPIN_RIGHTWIDTH, m_spRightWidth);
	DDX_Control(pDX, IDC_SPIN_RIGHTHOFFSET, m_spRightHOffset);
	DDX_Control(pDX, IDC_SPIN_RIGHTWOFFSET, m_spRightWOffset);
	DDX_Control(pDX, IDC_EDIT_INH, m_edInnerHeight);
	DDX_Control(pDX, IDC_EDIT_INW, m_edInnerWidth);
	DDX_Control(pDX, IDC_EDIT_OUH, m_edOutterHeight);
	DDX_Control(pDX, IDC_EDIT_OUW, m_edOutterWidth);
	DDX_Control(pDX, IDC_EDIT_BACKHEIGHT, m_edBackHeight);
	DDX_Control(pDX, IDC_EDIT_BACKWIDTH, m_edBackWidth);
	DDX_Control(pDX, IDC_EDIT_BACKHOFFSET, m_edBackHOffset);
	DDX_Control(pDX, IDC_EDIT_BACKWOFFSET, m_edBackWOffset);
	DDX_Control(pDX, IDC_SPIN_BACKHEIGHT, m_spBackHeight);
	DDX_Control(pDX, IDC_SPIN_BACKWIDTH, m_spBackWidth);
	DDX_Control(pDX, IDC_SPIN_BACKHOFFSET, m_spBackHOffset);
	DDX_Control(pDX, IDC_SPIN_BACKWOFFSET, m_spBackWOffset);
	DDX_Check(pDX, IDC_CHECK_BACKCOMP, m_bBackComp);
	DDX_Check(pDX, IDC_CHECK_FRONTCOMP, m_bFrontComp);
	DDX_Check(pDX, IDC_CHECK_LEFTCOMP, m_bLeftComp);
	DDX_Check(pDX, IDC_CHECK_RIGHTCOMP, m_bRightComp);
}

BEGIN_MESSAGE_MAP(CConfigCamera4Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FRONTHEIGHT, &CConfigCamera4Dlg::OnDeltaposSpinFrontheight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FRONTWIDTH, &CConfigCamera4Dlg::OnDeltaposSpinFrontwidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FRONTHOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinFronthoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FRONTWOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinFrontwoffset)
	ON_BN_CLICKED(IDOK, &CConfigCamera4Dlg::OnBnClickedOk)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LEFTHEIGHT, &CConfigCamera4Dlg::OnDeltaposSpinLeftheight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LEFTWIDTH, &CConfigCamera4Dlg::OnDeltaposSpinLeftwidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LEFTHOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinLefthoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_LEFTWOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinLeftwoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_BACKHEIGHT, &CConfigCamera4Dlg::OnDeltaposSpinBackheight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_BACKWIDTH, &CConfigCamera4Dlg::OnDeltaposSpinBackwidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_BACKHOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinBackhoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_BACKWOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinBackwoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RIGHTHEIGHT, &CConfigCamera4Dlg::OnDeltaposSpinRightheight)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RIGHTWIDTH, &CConfigCamera4Dlg::OnDeltaposSpinRightwidth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RIGHTHOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinRighthoffset)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RIGHTWOFFSET, &CConfigCamera4Dlg::OnDeltaposSpinRightwoffset)
	ON_BN_CLICKED(IDC_BUTTON_CONFRECT, &CConfigCamera4Dlg::OnBnClickedButtonConfrect)
	ON_BN_CLICKED(IDC_BUTTON_RELOAD, &CConfigCamera4Dlg::OnBnClickedButtonReload)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CConfigCamera4Dlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_FCONF, &CConfigCamera4Dlg::OnBnClickedButtonFconf)
	ON_BN_CLICKED(IDC_BUTTON_LCONF, &CConfigCamera4Dlg::OnBnClickedButtonLconf)
	ON_BN_CLICKED(IDC_BUTTON_RCONF, &CConfigCamera4Dlg::OnBnClickedButtonRconf)
	ON_BN_CLICKED(IDC_BUTTON_BCONF, &CConfigCamera4Dlg::OnBnClickedButtonBconf)
	ON_BN_CLICKED(IDC_CHECK_FRONTCOMP, &CConfigCamera4Dlg::OnBnClickedCheckFrontcomp)
	ON_BN_CLICKED(IDC_CHECK_BACKCOMP, &CConfigCamera4Dlg::OnBnClickedCheckBackcomp)
	ON_BN_CLICKED(IDC_CHECK_LEFTCOMP, &CConfigCamera4Dlg::OnBnClickedCheckLeftcomp)
	ON_BN_CLICKED(IDC_CHECK_RIGHTCOMP, &CConfigCamera4Dlg::OnBnClickedCheckRightcomp)
END_MESSAGE_MAP()


// CConfigCamera4Dlg 消息处理程序
#define BINGSPED(sp,ed,iv)	do{CString strv;\
								strv.Format("%1.2f",iv);\
								ed.SetWindowText(strv);\
								sp.SetRange(-1000,2000);\
								sp.SetRange32(-1000,2000);\
								sp.SetPos(iv*1000);}while(0)


BOOL CConfigCamera4Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	Reload();

	//m_spFrontHeight.SetBase(1);
	//m_spFrontHeight.SetBuddy(&m_edFrontHeight);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CConfigCamera4Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CConfigCamera4Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

#define CHGSPV(dire,param)	do{CString vstr;\
						int pos;\
						pos = pNMUpDown->iPos;\
						double newValue =  pos / 1000.0;\
						CString valueStr;\
						valueStr.Format("%1.3f",newValue);\
						m_ed##dire##param##.SetWindowTextA(valueStr);\
						Send##dire##Params();}while(0)

void CConfigCamera4Dlg::OnDeltaposSpinFrontheight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	/*
	int pos;
	int pos2=m_spFrontHeight.GetPos();
	CString dbgStr;
	dbgStr.Format("%x\n",pos2);
	OutputDebugString(dbgStr);
	pos = pNMUpDown->iPos;
	double newValue =  pos / 100.0;
	CString valueStr;
	valueStr.Format("%1.2f",newValue);
	m_edFrontHeight.SetWindowTextA(valueStr);
	*/
	CHGSPV(Front,Height);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinFrontwidth(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Front,Width);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinFronthoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Front,HOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinFrontwoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Front,WOffset);
	*pResult = 0;
}

#define SETJSONV(ed,name)	do{ed.GetWindowTextA(vastr);\
	root[name]=atof(vastr.GetBuffer());}while(0)

void CConfigCamera4Dlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	SaveConfig();
	CDialogEx::OnOK();
}


void CConfigCamera4Dlg::OnDeltaposSpinLeftheight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Left,Height);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinLeftwidth(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Left,Width);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinLefthoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Left,HOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinLeftwoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Left,WOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinBackheight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Back,Height);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinBackwidth(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Back,Width);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinBackhoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Back,HOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinBackwoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Back,WOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinRightheight(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Right,Height);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinRightwidth(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Right,Width);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinRighthoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Right,HOffset);
	*pResult = 0;
}


void CConfigCamera4Dlg::OnDeltaposSpinRightwoffset(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CHGSPV(Right,WOffset);
	*pResult = 0;
}

#define GETVL(dire,para)	m_ed##dire##para##.GetWindowTextA(tmpStr);m_f##dire##para##=atof(tmpStr.GetBuffer())
#define GET4VL(dire)	do{CString tmpStr;\
							GETVL(dire,Height);\
							GETVL(dire,Width);\
							GETVL(dire,HOffset);\
							GETVL(dire,WOffset);}while(0)

#define BUFFER_LENGTH 124
void CConfigCamera4Dlg::SendFrontParams(void)
{
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	GET4VL(Front);
	/*Send Front Params */
	mavlink_msg_front_params_pack(1, 200, &msg, m_fFrontHOffset, m_fFrontWOffset, m_fFrontHeight,m_fFrontWidth);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));
		
}


void CConfigCamera4Dlg::SendBackParams(void)
{
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	GET4VL(Back);
	/*Send Front Params */
	mavlink_msg_back_params_pack(1, 200, &msg, m_fBackHOffset, m_fBackWOffset, m_fBackHeight,m_fBackWidth);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));
}


void CConfigCamera4Dlg::SendLeftParams(void)
{	
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	GET4VL(Left);
	/*Send Front Params */
	mavlink_msg_left_params_pack(1, 200, &msg, m_fLeftHOffset, m_fLeftWOffset, m_fLeftHeight,m_fLeftWidth);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));	
}


void CConfigCamera4Dlg::SendRightParams(void)
{
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	GET4VL(Right);
	/*Send Front Params */
	mavlink_msg_right_params_pack(1, 200, &msg, m_fRightHOffset, m_fRightWOffset, m_fRightHeight,m_fRightWidth);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));
	
}


void CConfigCamera4Dlg::OnBnClickedButtonConfrect()
{
	// TODO: 在此添加控件通知处理程序代码
	CString tmpStr;
	m_edInnerHeight.GetWindowTextA(tmpStr);
	m_fInnerHeight = atof(tmpStr.GetBuffer());

	m_edInnerWidth.GetWindowTextA(tmpStr);
	m_fInnerWidth = atof(tmpStr.GetBuffer());

	m_edOutterHeight.GetWindowTextA(tmpStr);
	m_fOutterHeight = atof(tmpStr.GetBuffer());

	m_edOutterWidth.GetWindowTextA(tmpStr);
	m_fOutterWidth = atof(tmpStr.GetBuffer());
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	mavlink_msg_carrect_pack(1, 200, &msg, m_fInnerWidth, m_fInnerHeight, m_fOutterWidth,m_fOutterHeight);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));
	
	Sleep(100);
	SendRightParams();
	Sleep(100);
	SendLeftParams();
	Sleep(100);
	SendFrontParams();
	Sleep(100);
	SendBackParams();
	Sleep(100);
	SendCompConfig();
}


void CConfigCamera4Dlg::OnBnClickedButtonReload()
{
	// TODO: 在此添加控件通知处理程序代码
	Reload();
}


void CConfigCamera4Dlg::OnBnClickedButtonSave()
{
	// TODO: 在此添加控件通知处理程序代码
	SaveConfig();
}


void CConfigCamera4Dlg::Reload(void)
{
	Json::Features feature = Json::Features::strictMode();
	Json::Reader reader(feature);
	Json::Value root;
	std::string input = readInputTestFile("./oldvalue.json");
	if(input.empty()){
		
	}
	else{
		bool retCode = reader.parse(input,root);
		if(retCode){
			Json::Value::Members members(root.getMemberNames());
			for(Json::Value::Members::iterator it = members.begin();
				it != members.end();
				++it){
					const std::string &name = *it;
					printf("%s:%f\n",name.c_str(),root[name]);
			}
			m_fFrontHeight = root["frontheight"].asDouble();
			m_fFrontWidth = root["frontwidth"].asDouble();
			m_fFrontHOffset = root["fronthoff"].asDouble();
			m_fFrontWOffset = root["frontwoff"].asDouble();

			m_fBackHeight = root["backheight"].asDouble();
			m_fBackWidth = root["backwidth"].asDouble();
			m_fBackHOffset = root["backhoff"].asDouble();
			m_fBackWOffset = root["backwoff"].asDouble();

			m_fLeftHeight = root["leftheight"].asDouble();
			m_fLeftWidth = root["leftwidth"].asDouble();
			m_fLeftHOffset = root["lefthoff"].asDouble();
			m_fLeftWOffset = root["leftwoff"].asDouble();

			m_fRightHeight = root["rightheight"].asDouble();
			m_fRightWidth = root["rightwidth"].asDouble();
			m_fRightHOffset = root["righthoff"].asDouble();
			m_fRightWOffset = root["rightwoff"].asDouble();

			m_fInnerHeight = root["innerheight"].asDouble();
			m_fInnerWidth = root["innerwidth"].asDouble();
			m_fOutterHeight = root["outterheight"].asDouble();
			m_fOutterWidth = root["outterwidth"].asDouble();
		}
	} 
	BINGSPED(m_spFrontHeight,m_edFrontHeight,m_fFrontHeight);
	BINGSPED(m_spFrontWidth,m_edFrontWidth,m_fFrontWidth);
	BINGSPED(m_spFrontWOffset,m_edFrontWOffset,m_fFrontWOffset);
	BINGSPED(m_spFrontHOffset,m_edFrontHOffset,m_fFrontHOffset);

	BINGSPED(m_spBackHeight,m_edBackHeight,m_fBackHeight);
	BINGSPED(m_spBackWidth,m_edBackWidth,m_fBackWidth);
	BINGSPED(m_spBackWOffset,m_edBackWOffset,m_fBackWOffset);
	BINGSPED(m_spBackHOffset,m_edBackHOffset,m_fBackHOffset);

	BINGSPED(m_spLeftHeight,m_edLeftHeight,m_fLeftHeight);
	BINGSPED(m_spLeftWidth,m_edLeftWidth,m_fLeftWidth);
	BINGSPED(m_spLeftWOffset,m_edLeftWOffset,m_fLeftWOffset);
	BINGSPED(m_spLeftHOffset,m_edLeftHOffset,m_fLeftHOffset);

	BINGSPED(m_spRightHeight,m_edRightHeight,m_fRightHeight);
	BINGSPED(m_spRightWidth,m_edRightWidth,m_fRightWidth);
	BINGSPED(m_spRightWOffset,m_edRightWOffset,m_fRightWOffset);
	BINGSPED(m_spRightHOffset,m_edRightHOffset,m_fRightHOffset);
	CString strv;
	strv.Format("%d",int(m_fInnerHeight));
	m_edInnerHeight.SetWindowTextA(strv);

	strv.Format("%d",int(m_fInnerWidth));
	m_edInnerWidth.SetWindowTextA(strv);

	strv.Format("%d",int(m_fOutterHeight));
	m_edOutterHeight.SetWindowTextA(strv);

	strv.Format("%d",int(m_fOutterWidth));
	m_edOutterWidth.SetWindowTextA(strv);
}


void CConfigCamera4Dlg::SaveConfig(void)
{
	Json::FastWriter writer;
	Json::Value root;
	CString vastr;
	
	SETJSONV(m_edFrontHeight,"frontheight");
	SETJSONV(m_edFrontWidth,"frontwidth");
	SETJSONV(m_edFrontWOffset,"frontwoff");
	SETJSONV(m_edFrontHOffset,"fronthoff");

	SETJSONV(m_edBackHeight,"backheight");
	SETJSONV(m_edBackWidth,"backwidth");
	SETJSONV(m_edBackWOffset,"backwoff");
	SETJSONV(m_edBackHOffset,"backhoff");

	SETJSONV(m_edLeftHeight,"leftheight");
	SETJSONV(m_edLeftWidth,"leftwidth");
	SETJSONV(m_edLeftWOffset,"leftwoff");
	SETJSONV(m_edLeftHOffset,"lefthoff");

	SETJSONV(m_edRightHeight,"rightheight");
	SETJSONV(m_edRightWidth,"rightwidth");
	SETJSONV(m_edRightWOffset,"rightwoff");
	SETJSONV(m_edRightHOffset,"righthoff");

	m_edInnerWidth.GetWindowTextA(vastr);
	root["innerwidth"]=atof(vastr.GetBuffer());

	m_edInnerHeight.GetWindowTextA(vastr);
	root["innerheight"]=atof(vastr.GetBuffer());

	m_edOutterWidth.GetWindowTextA(vastr);
	root["outterwidth"]=atof(vastr.GetBuffer());

	m_edOutterHeight.GetWindowTextA(vastr);
	root["outterheight"]=atof(vastr.GetBuffer());

	std::string rewrite = writer.write(root);
	FILE * fout ;
	errno_t er = fopen_s(&fout,"./oldvalue.json","wt");
	if(er || !fout)
		return;
	fprintf(fout,rewrite.c_str());
	fclose(fout);
}


void CConfigCamera4Dlg::OnBnClickedButtonFconf()
{
	// TODO: 在此添加控件通知处理程序代码
	SendFrontParams();
}


void CConfigCamera4Dlg::OnBnClickedButtonLconf()
{
	// TODO: 在此添加控件通知处理程序代码
	SendLeftParams();
}


void CConfigCamera4Dlg::OnBnClickedButtonRconf()
{
	// TODO: 在此添加控件通知处理程序代码
	SendRightParams();
}


void CConfigCamera4Dlg::OnBnClickedButtonBconf()
{
	// TODO: 在此添加控件通知处理程序代码
	SendBackParams();
}


void CConfigCamera4Dlg::OnBnClickedCheckFrontcomp()
{
	// TODO: 在此添加控件通知处理程序代码
	SendCompConfig();
}


int CConfigCamera4Dlg::SendCompConfig(void)
{
	UpdateData(TRUE);
	mavlink_message_t msg;
	uint8_t buf[BUFFER_LENGTH];
	uint16_t len;
	int bytes_sent;
	uint32_t flags=0;
	flags |= m_bFrontComp?FRONT_COMP:0;
	flags |= m_bBackComp ? BACK_COMP:0;
	flags |= m_bLeftComp ? LEFT_COMP:0;
	flags |= m_bRightComp ? RIGHT_COMP:0;
	/*Send Config Cmd */
	mavlink_msg_config_cmd_pack(1, 200, &msg, flags);
	len = mavlink_msg_to_send_buffer(buf, &msg);
	bytes_sent = sendto(sock,(const char *) buf, len, 0, (struct sockaddr*)&peerAddr, sizeof(struct sockaddr_in));
	return 0;
}


void CConfigCamera4Dlg::OnBnClickedCheckBackcomp()
{
	// TODO: 在此添加控件通知处理程序代码
	SendCompConfig();
}


void CConfigCamera4Dlg::OnBnClickedCheckLeftcomp()
{
	// TODO: 在此添加控件通知处理程序代码
	SendCompConfig();
}


void CConfigCamera4Dlg::OnBnClickedCheckRightcomp()
{
	// TODO: 在此添加控件通知处理程序代码
	SendCompConfig();
}
