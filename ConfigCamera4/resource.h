//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 ConfigCamera4.rc 使用
//
#define IDD_CONFIGCAMERA4_DIALOG        102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_EDIT_FRONTHEIGHT            1000
#define IDC_EDIT_FRONTWIDTH             1001
#define IDC_EDIT_FRONTHOFFSET           1002
#define IDC_SPIN_FRONTHEIGHT            1003
#define IDC_SPIN_FRONTWIDTH             1004
#define IDC_SPIN_FRONTHOFFSET           1005
#define IDC_EDIT_INH                    1006
#define IDC_EDIT_INW                    1007
#define IDC_EDIT_OUH                    1008
#define IDC_EDIT_FRONTWOFFSET           1009
#define IDC_EDIT_OUW                    1010
#define IDC_SPIN_FRONTWOFFSET           1011
#define IDC_EDIT_RIGHTHEIGHT            1012
#define IDC_EDIT_RIGHTWIDTH             1013
#define IDC_EDIT_RIGHTHOFFSET           1014
#define IDC_SPIN_RIGHTHEIGHT            1015
#define IDC_SPIN_RIGHTWIDTH             1016
#define IDC_SPIN_RIGHTHOFFSET           1017
#define IDC_EDIT_RIGHTWOFFSET           1018
#define IDC_SPIN_RIGHTWOFFSET           1019
#define IDC_EDIT_LEFTHEIGHT             1020
#define IDC_EDIT_LEFTWIDTH              1021
#define IDC_EDIT_LEFTHOFFSET            1022
#define IDC_SPIN_LEFTHEIGHT             1023
#define IDC_SPIN_LEFTWIDTH              1024
#define IDC_SPIN_LEFTHOFFSET            1025
#define IDC_EDIT_LEFTWOFFSET            1026
#define IDC_SPIN_LEFTWOFFSET            1027
#define IDC_EDIT_BACKHEIGHT             1028
#define IDC_EDIT_BACKWIDTH              1029
#define IDC_EDIT_BACKHOFFSET            1030
#define IDC_SPIN_BACKHEIGHT             1031
#define IDC_SPIN_BACKWIDTH              1032
#define IDC_SPIN_BACKHOFFSET            1033
#define IDC_EDIT_BACKWOFFSET            1034
#define IDC_SPIN_BACKWOFFSET            1035
#define IDC_BUTTON_CONFRECT             1036
#define IDC_BUTTON2                     1037
#define IDC_BUTTON_RELOAD               1037
#define IDC_BUTTON_SAVE                 1038
#define IDC_BUTTON_FCONF                1039
#define IDC_BUTTON_LCONF                1040
#define IDC_BUTTON_RCONF                1041
#define IDC_BUTTON_BCONF                1042
#define IDC_CHECK_FRONTCOMP             1043
#define IDC_CHECK_BACKCOMP              1044
#define IDC_CHECK_LEFTCOMP              1045
#define IDC_CHECK_RIGHTCOMP             1046

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1047
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
