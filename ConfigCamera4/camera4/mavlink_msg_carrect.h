#pragma once
// MESSAGE CARRECT PACKING

#define MAVLINK_MSG_ID_CARRECT 4

MAVPACKED(
typedef struct __mavlink_carrect_t {
 float innerwidth; /*< innerwidth*/
 float innerheight; /*< innerheight*/
 float outterwidth; /*< outterwidth*/
 float otterheight; /*< outterheight*/
}) mavlink_carrect_t;

#define MAVLINK_MSG_ID_CARRECT_LEN 16
#define MAVLINK_MSG_ID_CARRECT_MIN_LEN 16
#define MAVLINK_MSG_ID_4_LEN 16
#define MAVLINK_MSG_ID_4_MIN_LEN 16

#define MAVLINK_MSG_ID_CARRECT_CRC 243
#define MAVLINK_MSG_ID_4_CRC 243



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CARRECT { \
    4, \
    "CARRECT", \
    4, \
    {  { "innerwidth", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_carrect_t, innerwidth) }, \
         { "innerheight", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_carrect_t, innerheight) }, \
         { "outterwidth", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_carrect_t, outterwidth) }, \
         { "otterheight", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_carrect_t, otterheight) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CARRECT { \
    "CARRECT", \
    4, \
    {  { "innerwidth", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_carrect_t, innerwidth) }, \
         { "innerheight", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_carrect_t, innerheight) }, \
         { "outterwidth", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_carrect_t, outterwidth) }, \
         { "otterheight", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_carrect_t, otterheight) }, \
         } \
}
#endif

/**
 * @brief Pack a carrect message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param innerwidth innerwidth
 * @param innerheight innerheight
 * @param outterwidth outterwidth
 * @param otterheight outterheight
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_carrect_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               float innerwidth, float innerheight, float outterwidth, float otterheight)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARRECT_LEN];
    _mav_put_float(buf, 0, innerwidth);
    _mav_put_float(buf, 4, innerheight);
    _mav_put_float(buf, 8, outterwidth);
    _mav_put_float(buf, 12, otterheight);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CARRECT_LEN);
#else
    mavlink_carrect_t packet;
    packet.innerwidth = innerwidth;
    packet.innerheight = innerheight;
    packet.outterwidth = outterwidth;
    packet.otterheight = otterheight;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CARRECT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CARRECT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
}

/**
 * @brief Pack a carrect message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param innerwidth innerwidth
 * @param innerheight innerheight
 * @param outterwidth outterwidth
 * @param otterheight outterheight
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_carrect_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   float innerwidth,float innerheight,float outterwidth,float otterheight)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARRECT_LEN];
    _mav_put_float(buf, 0, innerwidth);
    _mav_put_float(buf, 4, innerheight);
    _mav_put_float(buf, 8, outterwidth);
    _mav_put_float(buf, 12, otterheight);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CARRECT_LEN);
#else
    mavlink_carrect_t packet;
    packet.innerwidth = innerwidth;
    packet.innerheight = innerheight;
    packet.outterwidth = outterwidth;
    packet.otterheight = otterheight;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CARRECT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CARRECT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
}

/**
 * @brief Encode a carrect struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param carrect C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_carrect_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_carrect_t* carrect)
{
    return mavlink_msg_carrect_pack(system_id, component_id, msg, carrect->innerwidth, carrect->innerheight, carrect->outterwidth, carrect->otterheight);
}

/**
 * @brief Encode a carrect struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param carrect C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_carrect_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_carrect_t* carrect)
{
    return mavlink_msg_carrect_pack_chan(system_id, component_id, chan, msg, carrect->innerwidth, carrect->innerheight, carrect->outterwidth, carrect->otterheight);
}

/**
 * @brief Send a carrect message
 * @param chan MAVLink channel to send the message
 *
 * @param innerwidth innerwidth
 * @param innerheight innerheight
 * @param outterwidth outterwidth
 * @param otterheight outterheight
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_carrect_send(mavlink_channel_t chan, float innerwidth, float innerheight, float outterwidth, float otterheight)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARRECT_LEN];
    _mav_put_float(buf, 0, innerwidth);
    _mav_put_float(buf, 4, innerheight);
    _mav_put_float(buf, 8, outterwidth);
    _mav_put_float(buf, 12, otterheight);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARRECT, buf, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
#else
    mavlink_carrect_t packet;
    packet.innerwidth = innerwidth;
    packet.innerheight = innerheight;
    packet.outterwidth = outterwidth;
    packet.otterheight = otterheight;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARRECT, (const char *)&packet, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
#endif
}

/**
 * @brief Send a carrect message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_carrect_send_struct(mavlink_channel_t chan, const mavlink_carrect_t* carrect)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_carrect_send(chan, carrect->innerwidth, carrect->innerheight, carrect->outterwidth, carrect->otterheight);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARRECT, (const char *)carrect, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
#endif
}

#if MAVLINK_MSG_ID_CARRECT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_carrect_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  float innerwidth, float innerheight, float outterwidth, float otterheight)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_float(buf, 0, innerwidth);
    _mav_put_float(buf, 4, innerheight);
    _mav_put_float(buf, 8, outterwidth);
    _mav_put_float(buf, 12, otterheight);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARRECT, buf, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
#else
    mavlink_carrect_t *packet = (mavlink_carrect_t *)msgbuf;
    packet->innerwidth = innerwidth;
    packet->innerheight = innerheight;
    packet->outterwidth = outterwidth;
    packet->otterheight = otterheight;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARRECT, (const char *)packet, MAVLINK_MSG_ID_CARRECT_MIN_LEN, MAVLINK_MSG_ID_CARRECT_LEN, MAVLINK_MSG_ID_CARRECT_CRC);
#endif
}
#endif

#endif

// MESSAGE CARRECT UNPACKING


/**
 * @brief Get field innerwidth from carrect message
 *
 * @return innerwidth
 */
static inline float mavlink_msg_carrect_get_innerwidth(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field innerheight from carrect message
 *
 * @return innerheight
 */
static inline float mavlink_msg_carrect_get_innerheight(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field outterwidth from carrect message
 *
 * @return outterwidth
 */
static inline float mavlink_msg_carrect_get_outterwidth(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field otterheight from carrect message
 *
 * @return outterheight
 */
static inline float mavlink_msg_carrect_get_otterheight(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Decode a carrect message into a struct
 *
 * @param msg The message to decode
 * @param carrect C-struct to decode the message contents into
 */
static inline void mavlink_msg_carrect_decode(const mavlink_message_t* msg, mavlink_carrect_t* carrect)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    carrect->innerwidth = mavlink_msg_carrect_get_innerwidth(msg);
    carrect->innerheight = mavlink_msg_carrect_get_innerheight(msg);
    carrect->outterwidth = mavlink_msg_carrect_get_outterwidth(msg);
    carrect->otterheight = mavlink_msg_carrect_get_otterheight(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CARRECT_LEN? msg->len : MAVLINK_MSG_ID_CARRECT_LEN;
        memset(carrect, 0, MAVLINK_MSG_ID_CARRECT_LEN);
    memcpy(carrect, _MAV_PAYLOAD(msg), len);
#endif
}
