/** @file
 *  @brief MAVLink comm protocol built from camera4.xml
 *  @see http://mavlink.org
 */
#pragma once
 
#ifndef MAVLINK_VERSION_H
#define MAVLINK_VERSION_H

#define MAVLINK_BUILD_DATE "Tue Nov 15 2016"
#define MAVLINK_WIRE_PROTOCOL_VERSION "1.0"
#define MAVLINK_MAX_DIALECT_PAYLOAD_SIZE 16
 
#endif // MAVLINK_VERSION_H
