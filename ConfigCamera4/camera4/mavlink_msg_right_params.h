#pragma once
// MESSAGE RIGHT_PARAMS PACKING

#define MAVLINK_MSG_ID_RIGHT_PARAMS 3

MAVPACKED(
typedef struct __mavlink_right_params_t {
 float hoffset; /*< hoffset*/
 float woffset; /*< woffset*/
 float height; /*< height*/
 float width; /*< width*/
}) mavlink_right_params_t;

#define MAVLINK_MSG_ID_RIGHT_PARAMS_LEN 16
#define MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN 16
#define MAVLINK_MSG_ID_3_LEN 16
#define MAVLINK_MSG_ID_3_MIN_LEN 16

#define MAVLINK_MSG_ID_RIGHT_PARAMS_CRC 19
#define MAVLINK_MSG_ID_3_CRC 19



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_RIGHT_PARAMS { \
    3, \
    "RIGHT_PARAMS", \
    4, \
    {  { "hoffset", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_right_params_t, hoffset) }, \
         { "woffset", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_right_params_t, woffset) }, \
         { "height", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_right_params_t, height) }, \
         { "width", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_right_params_t, width) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_RIGHT_PARAMS { \
    "RIGHT_PARAMS", \
    4, \
    {  { "hoffset", NULL, MAVLINK_TYPE_FLOAT, 0, 0, offsetof(mavlink_right_params_t, hoffset) }, \
         { "woffset", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_right_params_t, woffset) }, \
         { "height", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_right_params_t, height) }, \
         { "width", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_right_params_t, width) }, \
         } \
}
#endif

/**
 * @brief Pack a right_params message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param hoffset hoffset
 * @param woffset woffset
 * @param height height
 * @param width width
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_right_params_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               float hoffset, float woffset, float height, float width)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RIGHT_PARAMS_LEN];
    _mav_put_float(buf, 0, hoffset);
    _mav_put_float(buf, 4, woffset);
    _mav_put_float(buf, 8, height);
    _mav_put_float(buf, 12, width);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN);
#else
    mavlink_right_params_t packet;
    packet.hoffset = hoffset;
    packet.woffset = woffset;
    packet.height = height;
    packet.width = width;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_RIGHT_PARAMS;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
}

/**
 * @brief Pack a right_params message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param hoffset hoffset
 * @param woffset woffset
 * @param height height
 * @param width width
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_right_params_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   float hoffset,float woffset,float height,float width)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RIGHT_PARAMS_LEN];
    _mav_put_float(buf, 0, hoffset);
    _mav_put_float(buf, 4, woffset);
    _mav_put_float(buf, 8, height);
    _mav_put_float(buf, 12, width);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN);
#else
    mavlink_right_params_t packet;
    packet.hoffset = hoffset;
    packet.woffset = woffset;
    packet.height = height;
    packet.width = width;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_RIGHT_PARAMS;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
}

/**
 * @brief Encode a right_params struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param right_params C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_right_params_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_right_params_t* right_params)
{
    return mavlink_msg_right_params_pack(system_id, component_id, msg, right_params->hoffset, right_params->woffset, right_params->height, right_params->width);
}

/**
 * @brief Encode a right_params struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param right_params C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_right_params_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_right_params_t* right_params)
{
    return mavlink_msg_right_params_pack_chan(system_id, component_id, chan, msg, right_params->hoffset, right_params->woffset, right_params->height, right_params->width);
}

/**
 * @brief Send a right_params message
 * @param chan MAVLink channel to send the message
 *
 * @param hoffset hoffset
 * @param woffset woffset
 * @param height height
 * @param width width
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_right_params_send(mavlink_channel_t chan, float hoffset, float woffset, float height, float width)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_RIGHT_PARAMS_LEN];
    _mav_put_float(buf, 0, hoffset);
    _mav_put_float(buf, 4, woffset);
    _mav_put_float(buf, 8, height);
    _mav_put_float(buf, 12, width);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RIGHT_PARAMS, buf, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
#else
    mavlink_right_params_t packet;
    packet.hoffset = hoffset;
    packet.woffset = woffset;
    packet.height = height;
    packet.width = width;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RIGHT_PARAMS, (const char *)&packet, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
#endif
}

/**
 * @brief Send a right_params message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_right_params_send_struct(mavlink_channel_t chan, const mavlink_right_params_t* right_params)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_right_params_send(chan, right_params->hoffset, right_params->woffset, right_params->height, right_params->width);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RIGHT_PARAMS, (const char *)right_params, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
#endif
}

#if MAVLINK_MSG_ID_RIGHT_PARAMS_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_right_params_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  float hoffset, float woffset, float height, float width)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_float(buf, 0, hoffset);
    _mav_put_float(buf, 4, woffset);
    _mav_put_float(buf, 8, height);
    _mav_put_float(buf, 12, width);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RIGHT_PARAMS, buf, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
#else
    mavlink_right_params_t *packet = (mavlink_right_params_t *)msgbuf;
    packet->hoffset = hoffset;
    packet->woffset = woffset;
    packet->height = height;
    packet->width = width;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_RIGHT_PARAMS, (const char *)packet, MAVLINK_MSG_ID_RIGHT_PARAMS_MIN_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN, MAVLINK_MSG_ID_RIGHT_PARAMS_CRC);
#endif
}
#endif

#endif

// MESSAGE RIGHT_PARAMS UNPACKING


/**
 * @brief Get field hoffset from right_params message
 *
 * @return hoffset
 */
static inline float mavlink_msg_right_params_get_hoffset(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  0);
}

/**
 * @brief Get field woffset from right_params message
 *
 * @return woffset
 */
static inline float mavlink_msg_right_params_get_woffset(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field height from right_params message
 *
 * @return height
 */
static inline float mavlink_msg_right_params_get_height(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field width from right_params message
 *
 * @return width
 */
static inline float mavlink_msg_right_params_get_width(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Decode a right_params message into a struct
 *
 * @param msg The message to decode
 * @param right_params C-struct to decode the message contents into
 */
static inline void mavlink_msg_right_params_decode(const mavlink_message_t* msg, mavlink_right_params_t* right_params)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    right_params->hoffset = mavlink_msg_right_params_get_hoffset(msg);
    right_params->woffset = mavlink_msg_right_params_get_woffset(msg);
    right_params->height = mavlink_msg_right_params_get_height(msg);
    right_params->width = mavlink_msg_right_params_get_width(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_RIGHT_PARAMS_LEN? msg->len : MAVLINK_MSG_ID_RIGHT_PARAMS_LEN;
        memset(right_params, 0, MAVLINK_MSG_ID_RIGHT_PARAMS_LEN);
    memcpy(right_params, _MAV_PAYLOAD(msg), len);
#endif
}
