#pragma once
// MESSAGE CONFIG_CMD PACKING

#define MAVLINK_MSG_ID_CONFIG_CMD 5

MAVPACKED(
typedef struct __mavlink_config_cmd_t {
 uint32_t display_flags; /*< System mode bitfield, see DISPLAY_FLAGS ENUM in mavlink/include/mavlink_types.h*/
}) mavlink_config_cmd_t;

#define MAVLINK_MSG_ID_CONFIG_CMD_LEN 4
#define MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN 4
#define MAVLINK_MSG_ID_5_LEN 4
#define MAVLINK_MSG_ID_5_MIN_LEN 4

#define MAVLINK_MSG_ID_CONFIG_CMD_CRC 223
#define MAVLINK_MSG_ID_5_CRC 223



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CONFIG_CMD { \
    5, \
    "CONFIG_CMD", \
    1, \
    {  { "display_flags", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_config_cmd_t, display_flags) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CONFIG_CMD { \
    "CONFIG_CMD", \
    1, \
    {  { "display_flags", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_config_cmd_t, display_flags) }, \
         } \
}
#endif

/**
 * @brief Pack a config_cmd message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param display_flags System mode bitfield, see DISPLAY_FLAGS ENUM in mavlink/include/mavlink_types.h
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_config_cmd_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t display_flags)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CONFIG_CMD_LEN];
    _mav_put_uint32_t(buf, 0, display_flags);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CONFIG_CMD_LEN);
#else
    mavlink_config_cmd_t packet;
    packet.display_flags = display_flags;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CONFIG_CMD_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CONFIG_CMD;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
}

/**
 * @brief Pack a config_cmd message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param display_flags System mode bitfield, see DISPLAY_FLAGS ENUM in mavlink/include/mavlink_types.h
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_config_cmd_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t display_flags)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CONFIG_CMD_LEN];
    _mav_put_uint32_t(buf, 0, display_flags);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CONFIG_CMD_LEN);
#else
    mavlink_config_cmd_t packet;
    packet.display_flags = display_flags;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CONFIG_CMD_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CONFIG_CMD;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
}

/**
 * @brief Encode a config_cmd struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param config_cmd C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_config_cmd_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_config_cmd_t* config_cmd)
{
    return mavlink_msg_config_cmd_pack(system_id, component_id, msg, config_cmd->display_flags);
}

/**
 * @brief Encode a config_cmd struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param config_cmd C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_config_cmd_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_config_cmd_t* config_cmd)
{
    return mavlink_msg_config_cmd_pack_chan(system_id, component_id, chan, msg, config_cmd->display_flags);
}

/**
 * @brief Send a config_cmd message
 * @param chan MAVLink channel to send the message
 *
 * @param display_flags System mode bitfield, see DISPLAY_FLAGS ENUM in mavlink/include/mavlink_types.h
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_config_cmd_send(mavlink_channel_t chan, uint32_t display_flags)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CONFIG_CMD_LEN];
    _mav_put_uint32_t(buf, 0, display_flags);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CONFIG_CMD, buf, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
#else
    mavlink_config_cmd_t packet;
    packet.display_flags = display_flags;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CONFIG_CMD, (const char *)&packet, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
#endif
}

/**
 * @brief Send a config_cmd message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_config_cmd_send_struct(mavlink_channel_t chan, const mavlink_config_cmd_t* config_cmd)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_config_cmd_send(chan, config_cmd->display_flags);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CONFIG_CMD, (const char *)config_cmd, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
#endif
}

#if MAVLINK_MSG_ID_CONFIG_CMD_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_config_cmd_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t display_flags)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, display_flags);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CONFIG_CMD, buf, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
#else
    mavlink_config_cmd_t *packet = (mavlink_config_cmd_t *)msgbuf;
    packet->display_flags = display_flags;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CONFIG_CMD, (const char *)packet, MAVLINK_MSG_ID_CONFIG_CMD_MIN_LEN, MAVLINK_MSG_ID_CONFIG_CMD_LEN, MAVLINK_MSG_ID_CONFIG_CMD_CRC);
#endif
}
#endif

#endif

// MESSAGE CONFIG_CMD UNPACKING


/**
 * @brief Get field display_flags from config_cmd message
 *
 * @return System mode bitfield, see DISPLAY_FLAGS ENUM in mavlink/include/mavlink_types.h
 */
static inline uint32_t mavlink_msg_config_cmd_get_display_flags(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Decode a config_cmd message into a struct
 *
 * @param msg The message to decode
 * @param config_cmd C-struct to decode the message contents into
 */
static inline void mavlink_msg_config_cmd_decode(const mavlink_message_t* msg, mavlink_config_cmd_t* config_cmd)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    config_cmd->display_flags = mavlink_msg_config_cmd_get_display_flags(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CONFIG_CMD_LEN? msg->len : MAVLINK_MSG_ID_CONFIG_CMD_LEN;
        memset(config_cmd, 0, MAVLINK_MSG_ID_CONFIG_CMD_LEN);
    memcpy(config_cmd, _MAV_PAYLOAD(msg), len);
#endif
}
